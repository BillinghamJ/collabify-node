http = require 'http'
socket_io = require 'socket.io'
fs = require 'fs'
_ = require 'underscore'

app = http.createServer ->
io = socket_io.listen app

data = {
	users: {}
	tracks: []
	current_id: 1
}

io.sockets.on 'connection', (socket) ->
	data.users[socket.id] = {}

	socket.on 'disconnect', ->
		delete data.users[socket.id]

	socket.on 'clear', ->
		data.tracks = []

	socket.on 'identify', (id) ->
		data.users[socket.id].id = id

	socket.on 'genres', (genres) ->
		data.users[socket.id].genres = genres

	socket.on 'add_tracks', (tracks) ->
		_.each tracks, (track) ->
			return if typeof track.uri == 'undefined'

			track = {
				id: data.current_id
				user: data.users[socket.id].id
				uri: track.uri
				genres: track.genres
			}

			data.tracks.push track
			data.current_id++

			_.each io.sockets.clients(), (client) ->
				return unless (->
					return true if data.users[client.id].id == track.user

					return true if _.some track.genres, (genre) ->
						_.contains data.users[client.id].genres, genre

					false
				)()

				client.emit 'new_track', track

	socket.on 'remove_track', (id) ->
		data.tracks = _.reject data.tracks, (t) -> t.id == id

	socket.on 'get_playlist', ->
		tracks = _.filter data.tracks, (track) ->
			return true if track.user == data.users[socket.id].id

			return true if _.some track.genres, (genre) ->
				_.contains data.users[socket.id].genres, genre

			false

		socket.emit 'playlist_index', tracks

app.listen process.env.PORT || 8080
